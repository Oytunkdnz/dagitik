import numpy
ort1, ss1 = 1, 0.6 # ortalama1 ve standart sapma1 
dizi1 = numpy.random.normal(ort1, ss1, 10000)
import matplotlib.pyplot as plt
count, bins, ignored = plt.hist(dizi1, 40, normed=True)
plt.plot(bins, 1/(ss1 * numpy.sqrt(2 * numpy.pi)) *
numpy.exp( - (bins - ort1)**2 / (2 * ss1**2) ),
linewidth=0) #linewidth'i 0'lamazsam veya silersem line chart ciziyor histogramin uzerine.
plt.show() #indis valorlerini numpy.digitize ile degistirebiliyoruz sanirim ama vaktim yetmedi malesef.
ort2, ss2 = -1, 0.7 # ortalama2 ve standart sapma2
dizi2 = numpy.random.normal(ort2, ss2, 10000)
import matplotlib.pyplot as plt
count, bins, ignored = plt.hist(dizi2, 40, normed=True)
plt.plot(bins, 1/(ss2 * numpy.sqrt(2 * numpy.pi)) *
numpy.exp(- (bins - ort2) ** 2 / (2 * ss2 ** 2)),
linewidth=0)
plt.show()
